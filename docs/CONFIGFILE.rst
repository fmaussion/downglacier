.. -*- rst -*- -*- restructuredtext -*-
.. This file should be written using restructured text conventions
.. default-role:: math

==============================
DownGlacier configuration file
==============================

The configuration file (example in ``downglacier/files/defaults.cfg``) contains
all the options defining a DownGlacier run. They are organized in sections
``[]``, subsections ``[[]]`` and options ``key = value``.


**[I/O]**
    | Input output options

    ``working_dir = /path/to/dir``
        Directory where to write the files. Will be created if non-existent
    ``predictand_file = /path/to/file``
        NetCDF file with the SEB/SMB predictands.
        See the ``files`` directory for an example.
    ``predictor_file = /path/to/file``
        NetCDF file with the reanalysis predictors.
        See the ``files`` directory for an example.
    ``keep_vars = mbtot, airtemp``
        Optional. List of variables you would like to downscale
        (the other variables are ignored).
        Not recommended for real-case runs.
    ``ignore_vars = mbtot, prcpsol``
        Optional. List of variables you would like to ignore
        (the other variables are downscaled).
        Not recommended for real-case runs.
    ``keep_zlevels = 4750``
        Optional. List of altitude slices you would like to downscale
        (the other altitudes are ignored).
    ``lag_pred = 0``
        Add lagged predictors. Set to an integer >= 1.
    ``principle_components = -1``
        Use the PCs of the predictors instead, which cumulative
        explained variance is reaching the given float ([0,1])
        setting this parameter to a negative value or to none
        wont make any transformation, setting it to 1 will
        select all the pcs, selecting it to a float between
        0.95 and 1. is commonly done
    ``multiprocessing = True``
        Use python multiprocessing for the predictor screening (recommended).
    ``processes = -1``
        Number of processors to use (-1 = all available)

**[CheckInput]**
    | Rather a kind of data exploration:
    |   - checks for collinearity in the predictors,
    |   - checks for auto-correlation and persistence in the predictands
    |   - checks that the SEB->SMB link is working well using a "perfect downscaling approach"

    ``run = True``
        Set to false if you want to skip this step

**[Screen]**
    | Predictor screening

    ``run = True``
        Set to false if you want to skip this step
    ``compute_cv = True``
        Lazy (wrong) cross-validation with a fixed model chosen with all data
        Set to false if you want to skip this step
    ``compute_outercv = True``
        Out-of-sample (true) cross-validation (computationaly more expensive)
        Set to false if you want to skip this step
    ``outercv = '4``
        | Outer cross-validation rule. Is composed of a letter and a number:
        |   `k`: k-fold cross-validation
        |   `w`: leave-one-out window, with the number being half-window size
    ``algo = lasso``
        Regression algorithm to use. Possible values are listed below. Each
        algorithm must be adjoined with a ``[[Keywords]]`` subjection, which
        options are desbribed below.

        **stepwise_regression_pcor**
            Predictors are selected iteratively as long as their partial
            correlation is significant at the threshold value

                ``threshold = 0.05``
                    stopping value (unit: p-value)
                ``maxpred = -1``
                    max number of predictors (see below for options -1 and 0)

        **stepwise_regression_cvrmse**
            Predictors are selected as long as they improve the cross validation
            RMSE of at least X%, where X is a threshold value

                ``threshold = 0.05``
                    stopping value (unit: p-value)
                ``maxpred = -1``
                    max number of predictors (see below for options -1 and 0)

        **lasso**
            "Least Absolute Shrinkage and Selection Operator", L1-regularized fit.

                ``selectorcv = k4``
                    cross-validation rule (see ``outercv`` above)

        **relaxed_lasso**
             Runs the lasso to identify the set of non-zero coefficients,
             and then applies the lasso again, but using only the predictors
             selected at the first step

                ``selectorcv = k4``
                    cross-validation rule (see ``outercv`` above)

        **constrained_lasso**
            Force the Lasso to use a limited number of predictors (usually not
            a good idea)

                ``selectorcv = k4``
                    cross-validation rule (see ``outercv`` above)
                ``maxpred = k4``
                    max number of predictors (see below for options -1 and 0)

        **lasso_ols**
            Runs the lasso to identify the set of non-zero coefficients, and then fits an
            unrestricted linear model to the selected set of features (usually not
            a good idea)

                ``selectorcv = k4``
                    cross-validation rule (see ``outercv`` above)

    ``[[Keywords]]``
        Keywords of the chosen regression model
    ``maxpred``
        | maxpred can take the values:
        |    `X \ge 1`: any number of predictors
        |    `0`      : a weak rule of thumb is applied (a minimum of 10
        |               degrees of freedom is required, i.e N-K-1>=10, with K the
        |               number of predictors)
        |    `-1`     : a stronger rule of thumb is applied (a minimum of five data points
        |               required to train one predictor). This is the default.


**[Regstats]**
    | Regression statisics: summary and plots

    ``run = True``
        Set to false if you want to skip this step
    ``plots = True``
        Set to false if you dont want to make the plots


**[Diagnostic]**
    | Compute the diagnostic variables

    ``run = True``
        Set to false if you want to skip this step
    ``plots = True``
        Set to false if you dont want to make the plots

**[Vkeys]**
    | Variables keymap. Simple way to rename the variables in the input files
    | to variables recognized by DownGlacier (keep as is)

**[Ikeys]**
    | Info variables keymap. Simple way to rename the variables in the input files
    | to variables recognized by DownGlacier (keep as is)