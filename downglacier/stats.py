""" Statistical tools for the DownGlacier program.

Besides some utilitary functions, two core interface objects are implemented.

The **LinearModel** interface wraps Statmodel's OLS and Scikit-Learn's LASSO models
under a common interface (i.e. "fit-predict" syntax). Future models should
implement it too.

The **ModelSelector** class is the core class which realizes the out-of sample
cross-validation, predictions and more. It is inherited by all DownGlacier
models (stepwise regression, LASSO, etc.).

Copyright: Fabien Maussion, 2014-2015

License: GPLv3+
"""

import numpy as np
import scipy.stats
import statsmodels.api as sm
import statsmodels.tsa.stattools as tsa
from statsmodels.tools import eval_measures as em
from sklearn.linear_model import Lasso, LassoCV, lasso_path
from sklearn.cross_validation import KFold, LeaveOneOut
import pandas as pd
import copy
from sklearn.utils import ConvergenceWarning
import warnings
warnings.filterwarnings("ignore", category=ConvergenceWarning)

def cor(x, y):
    """It is annoying that np.corrcoef returns a matrix, this returns a float."""

    return np.corrcoef(x, y)[0, 1]


def cor2(x, y):
    """Square of cor."""

    return cor(x, y)**2


def pcor(x, y, c):
    """Partial correlation (r2) of x and y when the effect of C is removed.
    
    Couldn't find a routine to do exactly this in statsmodels, so I
    rolled my own.
    
    y and x are the variables from which we want to compute the correlation,
    when the effect of the controlling variables in C is removed.
    
    The residuals after regressing X/Y on Ci are the parts of X/Y that 
    cannot be predicted by Ci. The partial correlation coefficient between
    Y and X adjusted for Ci is the correlation between these two sets of 
    residuals.
    
    Returns
    -------
    tuple (r2, pvalue)
    
    """

    # Degrees of freedom
    if len(c.shape) == 1:
        df = len(x) - 2 - 1
    else:
        df = len(x) - 2 - c.shape[1]

    # Dont forget the constant
    _c = sm.add_constant(c)
    fity = sm.OLS(y, _c).fit()
    fitx = sm.OLS(x, _c).fit()

    r = cor(fitx.resid, fity.resid)
    t = r / np.sqrt((1. - r ** 2) / df)
    p_e = scipy.stats.t.sf(np.abs(t), df) * 2  # error probability (two tailed)

    return r ** 2, p_e


def brierss(ref, refmodel, data):
    """Computes the Brier Skill Score.

    Parameters
    ----------
    ref: the reference data
    refmodel: the reference model data
    data: the data to compare to the reference model

    Returns
    -------
    the BSS
    """

    return 1. - (em.mse(ref, data) / em.mse(ref, refmodel))


def find_collinearity(df):
    """Finds collinearity between the predictors."""

    # Make a data array for the correlation matrix computation
    data = df.values
    vns = np.array(list(df.keys()))

    # Compute correlation matrix and keep the triangle values without diag
    cormat = np.corrcoef(data, rowvar=0)**2
    mask = np.zeros(cormat.shape)
    mask[np.triu_indices(len(vns), 1)] = 1

    # Histogram output for dummies, but too lazy to do it better now
    thresholds = np.array([0.95, 0.98, 1.0])
    out = dict()
    for t, t1 in zip(thresholds[0:-1], thresholds[1:]):
        key = '{}_{}'.format(t, t1)
        pno = np.where((cormat > t) & (cormat <= t1) & (mask == 1))
        out[key] = None if len(pno[0]) == 0 else list(sorted(zip(vns[pno[0]],
                                                                 vns[pno[1]])))

    return out

def input_statistics(df):
    """Compute some standard statistics about the predictands
    (normality, auto-correlation...).

    Parameters
    ----------
    df: the predictands time series in a DataFrame

    Returns
    -------
    a new dataframe
    """

    sdf = pd.DataFrame()
    sdf['unit'] = pd.Series([df[v].units for v in df.columns], index=df.columns)
    sdf['mean'] = df.mean()
    sdf['stddev'] = df.std()

    # extract form the tuples (double calc but whatev)
    sdf['shapirotest_w'] = df.apply(scipy.stats.shapiro).apply(lambda x: x[0])
    sdf['shapirotest_pval'] = df.apply(scipy.stats.shapiro).apply(lambda x: x[1])
    # Anderson test
    sdf['anderson_05%'] = df.apply(scipy.stats.anderson).apply(lambda x: x[0] > x[1][2])
    sdf['anderson_10%'] = df.apply(scipy.stats.anderson).apply(lambda x: x[0] > x[1][1])

    # unfortunately those nice Pandas tricks are not working with acf
    nl = 12
    d = np.zeros((nl, len(df.columns)))
    for i, vn in enumerate(df):
        d[:,i] = tsa.acf(df[vn], nlags=nl)[1:] ** 2
    for i in np.arange(nl):
        sdf['autocor_{}'.format(i+1)] = d[i,:]

    return sdf


def l1o_climatology(data):
    """Computes the Leave-one-out climatology of a time serie.

    Parameters
    ----------
    data: the time serie

    Returns
    -------
    the climatology timeserie, same length as data
    """

    ny, r = divmod(len(data), 12)
    if r != 0:
        raise ValueError('Number of years should be straight.')

    rd = np.reshape(data, (ny, 12))
    lc = np.array([])
    for idx, _ in LeaveOneOut(ny):
        lc = np.append(lc, np.mean(rd[idx, :], axis=0))

    return lc


def correction_factor(data, refdata, vartype, cv=None):
    """Computes a correction factor so that mean(data) = mean(ref)."""

    if vartype == 'fullmodel_ts':
        return np.mean(refdata) / np.mean(data)

    # for other models it's the same and based on the outer cv
    corf = np.zeros(len(data))
    for inidx, teidx in cv_iterator(cv, len(data)):
        corf[teidx] = np.mean(refdata[inidx]) / np.mean(data[inidx])

    return corf


def maxpredrule(maxpred, n):
    """Decide the max number of predictors based on user rules.

    Accepted values are:
     X: any number of your choice between 1 and N-1 where N is the number of
        observations
     0: in this case, a weak rule of thumb is applied (a minimum of 10
         degrees of freedom is required, i.e N-K-1>=10, with K the
         number of predictors)
    -1: a stronger rule of thumb is applied (a minimum of five data points
        required to train one predictor). This is the default.

    Parameters
    ----------
    maxpred: the user choice (integer)
    n: the length of the time-serie

    Returns
    -------
    an integer (the max number of allowed predictors)
    """

    m = maxpred
    if maxpred == -1: # strong rule of thumb
        m, _ = divmod(n, 5)
    elif maxpred == 0: # weak rule of thumb
        m = n - 11
    elif (maxpred >= (n - 1)) or (maxpred < -1):
        raise ValueError('Illegal maxpred: {}'.format(maxpred))

    return m

def cv_iterator(cv, n):
    """Simple factory: decide which cross-validation iterator to use.

    The rule is based on the string cv, for example::
        'k4': 4-fold cross-validation
        'w2': leave-window-out cross val, halfsize=2 (thus window size = 5)

    Parameters
    ----------
    cv: the user choice (string)
    n: the length of the time-serie

    Returns
    -------
    a cross-validation iterator
    """

    if cv[0] == 'w': # lwo ite
        m = LeaveWinOutIte(n, int(cv[1:]))
    elif cv[0] == 'k': # Kfold
        m = KFold(n, n_folds=int(cv[1:]))
    else:
        raise ValueError('Illegal cv: {}'.format(cv))

    return m


def roundsum(array):
    """Rounding a floating point array to integers while making sure
    that the sum is preserved.

    It simply rounds *down* all numbers and then rounds up as many deserving
    candidates as possible.

    Parameters
    ----------
    array : numpy.array
        The array to round up (must sum up to an int).

    Returns
    -------
    numpy.array of rounded numbers.
    """


    sign = np.sign(array)
    _array = np.abs(array)
    expected_sum = np.float32(np.sum(_array)) # This was for a bug
    if expected_sum != np.rint(expected_sum):
        raise ArithmeticError("The sum of array should be an int")

    out = np.floor(_array)
    diff = out + 1 - _array

    for so in np.argsort(diff):
        if np.sum(out) == expected_sum: break
        out[so] += 1

    return out * sign


class LeaveWinOutIte(object):
    """
    Leave-Window-Out cross validation iterator:
    Provides train/test indexes to split data in train test sets
    """
    def __init__(self, n, p):
        """
        Leave-Window-Out cross validation iterator:
        Provides train/test indexes to split data in train test sets

        Parameters
        ----------
        n: int
            Total number of elements
        p: int
            Window half-size

        Examples
        --------
        >>> from downglacier.stats import LeaveWinOutIte
        >>> lwo = LeaveWinOutIte(4, 1)
        >>> for train_index, test_index in lwo:
        ...    print("TRAIN:", train_index, "TEST:", test_index)
        TRAIN: [False False  True  True] TEST: [ True False False False]
        TRAIN: [False False False  True] TEST: [False  True False False]
        TRAIN: [ True False False False] TEST: [False False  True False]
        TRAIN: [ True  True False False] TEST: [False False False  True]
        """
        assert p<((n-1)/2), ValueError('cannot have p=%d greater than '
                                       '%d'%(p,((n-1)/2)))
        self.n = n
        self.p = p

    def __iter__(self):
        n = self.n
        p = self.p
        for i in range(n):
            test_index = np.zeros(n, dtype=np.bool)
            test_index[i] = True
            train_index  = np.ones(n, dtype=np.bool)
            train_index[np.clip(i-p, 0, n):(i+p+1)] = False
            yield train_index, test_index

    def __repr__(self):
        return '%s.%s(n=%i, p=%i)' % (
                                self.__class__.__module__,
                                self.__class__.__name__,
                                self.n,
                                self.p,
                                )


def crossval_ts(x, y, model, crossvalite):
    """Computes the cross-validation timeserie wit
    a defined model (in-sample, 'bad' CV).

    Parameters
    ----------
    x: 1d (N,) predictand array
    y: 2d (N, P,) predictors array
    model: a LinearModel instance
    crossvalite: the cross-validation iterator to use

    Returns
    -------
     1d (N,) timeserie
    """

    # Modelled Timeserie
    mts = np.array([])
    for inidx, teidx in crossvalite:
        model.fit(x[inidx], y[inidx, :])
        mts = np.append(mts, model.predict(y[teidx, :]))

    return mts

class LinearModel():
    """ESD model interface to be implemented by real models (Lasso, OLS).

    It's purpose is simply to wrap sklearn's and statsmodel's models under a
    common syntax."""

    def fit(self, vardata, preds):
        """Fit (i.e calibrate) the model.

        Parameters:
        ----------
        vardata: 1d (N,) array of the predictand variable
        preds: 2d (N, P,) array of the predictors (P=number of predictors)
        """
        raise NotImplementedError

    def predict(self, preds):
        """Make predictions.

        Parameters:
        ----------
        preds: 2d (NL, P,) array of the predictors
        """
        raise NotImplementedError

    def selpreds_summary(self, vns):
        """Returns the selected predictors in a human readable manner.

        Parameters:
        ----------
        vns: 1d (P,) array of the predictor names

        Returns:
        --------
        Two strings: (pred_names, pred_names_summary)
        """
        raise NotImplementedError

    @property
    def fittedvalues(self):
        """Returns the model fitted values after the fit.

        Returns:
        --------
        1d (N,) array of the fitted values
        """
        raise NotImplementedError


class OLSModel(LinearModel):
    """Wrapper for statsmodels' OLS model."""

    def __init__(self, selpreds):
        """ Instanciate.

        Parameters
        ----------
        selpreds: a 1d array of the indices of the predictors to use
                  in the model. See fit() below
        """
        self.model = None
        self.selp = selpreds

    def fit(self, vardata, preds):
        self.model = sm.OLS(vardata, preds[:, self.selp], hasconst=True).fit()

    def predict(self, preds):
        return self.model.predict(preds[:, self.selp])

    @property
    def fittedvalues(self):
        return self.model.fittedvalues

    def selpreds_summary(self, vns):
        selp = vns[self.selp][1:] # No need for intercept
        pvals = self.model.pvalues[1:] # No need for intercept
        aso = pvals.argsort()
        pvals = pvals[aso]
        coefs = self.model.params[1:][aso]
        selp = selp[aso]
        str = []
        for i, pv in enumerate(pvals):
            pre = '+' if coefs[i] >= 0 else '-'
            if pv > 0.01 and pv <= 0.05:
                suf = '*'
            elif pv > 0.001 and pv <= 0.01:
                suf = '**'
            elif pv <= 0.001:
                suf = '***'
            else:
                suf = ''
            str.append(pre + selp[i] + suf)
        toprint = ', '.join(str)
        return selp, toprint


class LassoModel(LinearModel):
    """Wrapper for sklearn' LASSO model."""

    def __init__(self, alpha):
        """ Instanciate.

        Parameters
        ----------
        alpha: the penalization parameter of the Lasso model
        """
        self.model = Lasso(alpha=alpha)
        self.fitted = None

    def fit(self, vardata, preds):
        self.model = self.model.fit(preds, vardata)
        self.fitted = self.predict(preds) # for later

    def predict(self, preds):
        return self.model.predict(preds)

    @property
    def fittedvalues(self):
        return self.fitted

    def selpreds_summary(self, vns):
        pok = np.where(self.model.coef_ != 0)[0]
        coefs = self.model.coef_[pok]
        # normalize to percent
        coefs = coefs / np.sum(np.abs(coefs)) * 100
        selp = vns[pok]
        # Sorted by "strength"
        aso = np.abs(coefs).argsort()[::-1]
        selp = selp[aso]
        coefs = coefs[aso]
        str = []
        coefs = roundsum(coefs)
        for i, pv in enumerate(coefs):
            num = '{:+d} '.format(np.rint(coefs[i]).astype(int))
            str.append(num + selp[i])
        toprint = ', '.join(str)
        return selp, toprint


class RelaxedLassoModel(LinearModel):
    """Wrapper for a "reduced" LASSO model."""

    def __init__(self, alpha, selpreds):
        """ Instanciate.

        Parameters
        ----------
        alpha: the penalization parameter of the Lasso model
        selpreds: a 1d array of the indices of the predictors to use
                  in the model. See fit() below
        """
        self.model = Lasso(alpha=alpha)
        self.selp = selpreds
        self.fitted = None

    def fit(self, vardata, preds):
        self.model = self.model.fit(preds[:, self.selp], vardata)
        self.fitted = self.predict(preds) # for later

    def predict(self, preds):
        return self.model.predict(preds[:, self.selp])

    @property
    def fittedvalues(self):
        return self.fitted

    def selpreds_summary(self, vns):
        """returns a list of selected predictors, and also a string for prints"""
        _vns = vns[self.selp]
        pok = np.where(self.model.coef_ != 0)[0]
        coefs = self.model.coef_[pok]
        selp = _vns[pok]
        # normalize to percent
        coefs = coefs / np.sum(np.abs(coefs)) * 100
        # Sorted by "strength"
        aso = np.abs(coefs).argsort()[::-1]
        selp = selp[aso]
        coefs = coefs[aso]
        str = []
        coefs = roundsum(coefs)
        for i, pv in enumerate(coefs):
            num = '{:+d}'.format(np.rint(coefs[i]).astype(int))
            str.append(num + selp[i])
        toprint = ' '.join(str)
        return selp, toprint


class ModelSelector(object):
    """Provides a common interface for all screening algorithms.

    All subclasses have to implement one and only one function: _screen(),
    which is model dependant. All other tasks are the same for all
    models and implemented by the ModelSelector.

    The tasks are distributed in separate functions that may or may not
    be called at run-time depending on user requirements. The outer-cv
    is not *always* computed for example.
    """

    def __init__(self, glaciervar, predsdf, outercv='k4'):
        """ Instanciate.

        Parameters
        ----------
        glaciervar: a downglacier.io.GlacierVar instance
        predsdf: a DataFrame containing the predictor timeseries
        outercv: the user-defined cross-validation rule (integer >= 0, see doc)
        """

        self.predsdf = predsdf
        # shortcuts
        self._vardata = glaciervar.ref_ts.values
        self._preds = self.predsdf.values
        # because we will need the iterator many times
        self.outercv = list(cv_iterator(outercv, len(self._vardata)))
        # This is filled at run-time
        self.fullmodel = None
        self.fullmodel_ts = None
        self.cv_ts = None
        self.outercv_ts = None

    def _screen(self, vardata, preds):
        """Chooses a model based on a predictand/predictor pair.

        This function is called once for the full model fit, and more if
        the outer cv has to be computed (each call with a subset of the data).

        It has to be implemented by it's subclasses, see examples below.

        Returns
        -------
        A LinearModel instance
        """
        raise NotImplementedError()

    def compute_fullmodel(self):
        """Defines the full model by screening all available data."""
        self.fullmodel = self._screen(self._vardata,
                                      self._preds)
        self.fullmodel.fit(self._vardata,
                           self._preds)
        self.fullmodel_ts = self.fullmodel.fittedvalues

    def compute_cv(self):
        """Computes the inner (bad) cross-validation timeserie."""
        self.cv_ts = crossval_ts(self._vardata, self._preds,
                                 copy.deepcopy(self.fullmodel), self.outercv)

    def compute_outercv(self):
        """Computes the outer (good) cross-validation timeserie.

        A new model is selected for each cv fold."""

        # Modelled Timeserie
        mts = np.array([])
        for inidx, teidx in self.outercv:
            innermodel = self._screen(self._vardata[inidx],
                                      self._preds[inidx, :])
            innermodel.fit(self._vardata[inidx],
                           self._preds[inidx, :])
            mts = np.append(mts, innermodel.predict(self._preds[teidx, :]))

        self.outercv_ts = mts

    def predict(self, predsdf):
        """Computes the predicted timeserie."""

        # Necessary check, but rather ugly. This should not be the business
        # of the model selector. TODO: I think a better design is possible
        if isinstance(self.fullmodel, OLSModel):
            return self.fullmodel.predict(sm.add_constant(predsdf).values)
        else:
            return self.fullmodel.predict(predsdf.values)


    def selpreds_summary(self):
        """Returns the selected predictors in a human readable manner."""
        return self.fullmodel.selpreds_summary(self.predsdf.columns)



class DummyModelSelector(ModelSelector):
    """For test purposes: selects one single predictor."""

    def __init__(self, glaciervar, predsdf, outercv='k4'):
        # Dont forget to add the constant
        ModelSelector.__init__(self, glaciervar, sm.add_constant(predsdf),
                               outercv=outercv)

    def _screen(self, vardata, preds):
        # Find the highest r2
        r2 = np.array([])
        for i in np.arange(1, preds.shape[1], 1):
            r2 = np.append(r2, cor2(vardata, preds[:,i]))
        selp = np.array([0, r2.argmax()+1]) # for the constant
        return OLSModel(selp)


class PartialCorModelSelector(ModelSelector):
    """Mixed screening based on partial correlation."""

    def __init__(self, glaciervar, predsdf, outercv='k4',
                 threshold=0.05, maxpred=-1):

        self.threshold = threshold
        self.maxpred = maxpred

        # Dont forget the add constant
        ModelSelector.__init__(self, glaciervar, sm.add_constant(predsdf),
                               outercv=outercv)

    def _screen(self, vardata, preds):
        """Screen the best predictors using mixed partial correlation.

        Works as follows:
            1. first, select the most significant predictor
            2. then, loop over the remaining predictors and compute the partial
               correlation of each with the effect of the previously selected
               predictors removed
            3. keep the most significant one
            4. if at any point the p-value for one of the predictors rises above
               pthreshold, it is removed
            5. repeat 2. to 4. forward and backward steps until all predictors
               in the model have a sufficiently low p-value, and all predictors
               outside the model would have a large p-value if added"""

        n = len(vardata)
        maxp = maxpredrule(self.maxpred, n)

        # Selected ids (output)
        selp = np.array([0]) # for the constant

        # First find the higher r2, this should be easy
        r2 = np.array([])
        for i in np.arange(1, preds.shape[1], 1):
            r2 = np.append(r2, cor2(vardata, preds[:, i]))
        selp = np.append(selp, r2.argmax()+1) # for the constant
        first_pred = r2.argmax()+1 # keep it in case we need it at the end

        # Loop over the remaining predictors, as long as threshold is not reached
        screen = True
        count = 1
        # I need some stopping counter because, very rarely, predictors pairs
        # could be interchangeable endlessly
        while screen and len(selp) < maxp and count < 100:

            ids = np.array([], dtype=np.int)  # candidates
            mse = np.array([])  # corr coefs
            fitres = np.array([])   # modelresults

            # Compute OLS for all remaining possible predictors
            for i in np.arange(preds.shape[1]):
                if i in selp: continue
                # Predictors we already have and from which we remove influence
                C = preds[:, np.append(selp, i)]
                res = sm.OLS(vardata, C).fit()
                # Is the last predictor significant? If not, do not consider it
                if res.pvalues[-1] > self.threshold: continue
                mse = np.append(mse, em.mse(vardata, res.fittedvalues))
                ids = np.append(ids, i)
                fitres = np.append(fitres, res)

            # Do we even need to go further?
            if len(mse) == 0:
                screen = False
                continue

            # keep the best model for continuation and check the new pvalues
            am = mse.argmin()
            res = fitres[am]
            selp = np.append(selp, ids[am])
            pok = np.where(res.pvalues[1:] > self.threshold)[0]
            if len(pok) != 0:
                selp = np.delete(selp, pok+1) # for the constant

            count += 1

        if len(selp) == 1:
            # Maybe they really are all shit, we need at least one
            selp = np.append(selp, first_pred)

        return OLSModel(selp)


class CrossvalModelSelector(ModelSelector):
    """Mixed screening based on crossval RMSE."""

    def __init__(self, glaciervar, predsdf, outercv='k4',
                 threshold=0.01, maxpred=-1, selectorcv='k4'):

        self.selectorcv = selectorcv
        self.threshold = threshold
        self.maxpred = maxpred

        # Dont forget to add a constant
        ModelSelector.__init__(self, glaciervar, sm.add_constant(predsdf),
                               outercv=outercv)

    def _screen(self, vardata, preds):
        """Screen the predictors using mixed screening crossval RMSD selection."""

        n = len(vardata)
        maxp = maxpredrule(self.maxpred, n)
        cv = list(cv_iterator(self.selectorcv, n))

        # Selected ids (output)
        selp = np.array([0]) # for the constant

        # First find the higher r2, this should be easy
        r2 = np.array([])
        for i in np.arange(1, preds.shape[1], 1):
            r2 = np.append(r2, cor2(vardata, preds[:, i]))
        selp = np.append(selp, r2.argmax()+1) # for the constant

        # Loop over the remaining predictors, as long as threshold is not reached
        count = 1
        improvement = 1. # in %
        prevrmsd = 9999. # there should be a better way
        eps = 0.00001 # for the improbable case of a perfect model (testing)

        # I need some stopping counter because, very rarely, predictors pairs
        # could be interchangeable endlessly
        while (improvement >= self.threshold) and (len(selp) < maxp) \
                and (prevrmsd > eps) and (count < 100):

            rmsd = np.array([])
            ids = np.array([], dtype=np.int)  # candidates

            # Compute cv RMSDs for all remaining possible predictors
            for i in np.arange(preds.shape[1]):
                if i in selp: continue
                # Predictors we already have and from which we remove influence
                model = OLSModel(np.append(selp, i))
                cvts = crossval_ts(vardata, preds, model, cv)
                rmsd = np.append(rmsd, em.rmse(vardata, cvts))
                ids = np.append(ids, i)

            if len(rmsd) == 0: # no predictors left
                count = 9999
                continue # get out the loop

            # From which we keep the best for continuation
            am = rmsd.argmin()

            # improvement to before
            improvement = (prevrmsd - rmsd[am]) /  prevrmsd

            if improvement >= self.threshold:
                selp = np.append(selp, ids[am])
            else:
                count = 9999
                continue # get out the loop

            # before next round, check if we can remove preds without RMSE lost
            prevrmsd = rmsd[am]
            check = True
            while check:
                rmsd1 = np.array([])
                for k, id in enumerate(selp):
                    if id == 0: continue
                    model = OLSModel(np.delete(selp, k))
                    cvts = crossval_ts(vardata, preds, model, cv)
                    rmsd1 = np.append(rmsd1, em.rmse(vardata, cvts))

                am = rmsd1.argmin()
                if rmsd1[am] > prevrmsd:
                    check = False
                else:
                    selp = np.delete(selp, am+1)
                    prevrmsd = rmsd1[am]

            count += 1

        return OLSModel(selp)


class LassoModelSelector(ModelSelector):
    """Chooses the penalization param for Lasso."""

    def __init__(self, glaciervar, predsdf, outercv='k4', selectorcv='k4'):
        self.selectorcv = selectorcv
        # No need for add constant in sklearn
        ModelSelector.__init__(self, glaciervar, predsdf, outercv=outercv)

    def _screen(self, vardata, preds):
        cv = cv_iterator(self.selectorcv, len(vardata))
        lassocv = LassoCV(cv=cv, max_iter=10000).fit(preds, vardata)
        if not hasattr(self, 'lassocv'):
             # just for later plots
            lassocv._preds = preds
            self.lassocv = lassocv
        return LassoModel(lassocv.alpha_)


class RelaxedLassoModelSelector(ModelSelector):
    """works in two steps: Lasso is run once to select potential candidates
    and then a second time without the noisy competitors."""

    def __init__(self, glaciervar, predsdf, outercv='k4', selectorcv='k4'):
        self.selectorcv = selectorcv
        # No need for add constant in sklearn
        ModelSelector.__init__(self, glaciervar, predsdf, outercv=outercv)

    def _screen(self, vardata, preds):
        cv = cv_iterator(self.selectorcv, len(vardata))
        lassocv = LassoCV(cv=cv).fit(preds, vardata)
        selp = np.where(lassocv.coef_ != 0)[0]

        cv = cv_iterator(self.selectorcv, len(vardata))
        lassocv = LassoCV(cv=cv, max_iter=10000).fit(preds[:, selp], vardata)
        if not hasattr(self, 'lassocv'):
             # just for later plots
            lassocv._preds = preds[:, selp]
            self.lassocv = lassocv

        return RelaxedLassoModel(lassocv.alpha_, selp)


class ConstrainedLassoModelSelector(ModelSelector):
    """Works in two steps: Lasso is run once to select potential candidates
    and then a second time for the chosen alphas with npreds constrained."""

    def __init__(self, glaciervar, predsdf, outercv='k4', selectorcv='k4',
                 maxpred=-1):
        self.selectorcv = selectorcv
        self.maxpred = maxpred
        # No need for add constant in sklearn
        ModelSelector.__init__(self, glaciervar, predsdf, outercv=outercv)

    def _screen(self, vardata, preds):

        n = len(vardata)
        maxp = maxpredrule(self.maxpred, n)

        cv = cv_iterator(self.selectorcv, n)
        lassocv = LassoCV(cv=cv, n_alphas=200, max_iter=10000).fit(preds, vardata)

        # Compute the path again. Seems quite unefficient but lassoCV doesnt
        # provide the coefficients
        coef_path = np.zeros((len(lassocv.coef_), len(lassocv.alphas_)))
        for i, alp in enumerate(lassocv.alphas_):
            lassot = Lasso(alpha=alp, fit_intercept=True).fit(preds, vardata)
            coef_path[:, i] = lassot.coef_

        keep_alphas = np.where(np.sum(coef_path != 0, 0) <= maxp)
        cv = cv_iterator(self.selectorcv, len(vardata))
        lassocv = LassoCV(cv=cv, alphas=lassocv.alphas_[keep_alphas]).fit(preds, vardata)
        if not hasattr(self, 'lassocv'):
             # just for later plots
            lassocv._preds = preds
            self.lassocv = lassocv

        return LassoModel(lassocv.alpha_)


class OLSLassoModelSelector(ModelSelector):
    """works in two steps: Lasso is run once to select potential candidates
    and then an OLS is fitted with the selected preds."""

    def __init__(self, glaciervar, predsdf, outercv='k4', selectorcv='k4'):
        self.selectorcv = selectorcv
        # Dont forget to add a constant
        ModelSelector.__init__(self, glaciervar, sm.add_constant(predsdf), outercv=outercv)


    def _screen(self, vardata, preds):
        cv = cv_iterator(self.selectorcv, len(vardata))
        lassocv = LassoCV(cv=cv, max_iter=10000).fit(preds[:, 1:], vardata) #without constant
        selp = np.where(lassocv.coef_ != 0)[0]
        selp = np.insert(selp+1, 0, 0)  # for the constant
        return OLSModel(selp)


class DiagnosticModel(object):
    """Mimics a ModelSelector for duck-typing purposes."""

    def __init__(self):
        self.fullmodel = None
        self.fullmodel_ts = None
        self.cv_ts = None
        self.outercv_ts = None

    def selpreds_summary(self):
        return '-', '-'