"""Plot functions.

Copyright: Fabien Maussion, 2014-2015

License: GPLv3+
"""

import matplotlib.pyplot as plt
from matplotlib.dates import YearLocator, MonthLocator, DateFormatter
import numpy as np
import statsmodels.api as sm
from sklearn.linear_model import Lasso
import downglacier.stats as dgstats
import seaborn


def __parseunit(unit):
    "Transforms a CF compliant unit string to matplotlibs latex maths"

    ostr = unit.split(" ")
    for i, o in enumerate(ostr):
        if '^' in o:
            ostr[i] = o.replace('^', '$^{') + '}$'
    return ' '.join(ostr)


def __timeserie(time, datalist, title='', yunits='', file=None):

    fig, ax = plt.subplots()
    fig.set_size_inches(9, 3)
    for dd in datalist:
        plt.plot_date(time, dd['d'], linestyle=dd['ls'], label=dd['lab'])
    plt.title(title)
    plt.ylabel(__parseunit(yunits))

    # format the ticks
    ax.xaxis.set_major_locator(YearLocator())
    ax.xaxis.set_major_formatter(DateFormatter('%Y'))
    ax.xaxis.set_minor_locator(MonthLocator())

    legend = plt.legend(loc='best')
    plt.tight_layout()
    file.savefig()
    plt.close()


def regplots(res, varname=None, file=None):
    """OLS regression plots"""

    # mo = res.model
    # plt.figure(figsize=(9, 6))
    #
    # plt.suptitle(varname + ': regression plots', fontsize=16)
    #
    # ax = plt.subplot(221)
    # plt.scatter(mo.endog, res.fittedvalues)
    # plt.title('Model vs Fitted (R2adj={0:.2f})'.format(res.rsquared_adj), fontsize=12)
    # plt.xlabel('Observations')
    # plt.ylabel('Fitted')
    #
    # lims = [np.min([ax.get_xlim(), ax.get_ylim()]),  # minmax of both axes
    #         np.max([ax.get_xlim(), ax.get_ylim()])]
    #
    # # now plot both limits against each other
    # ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)
    # ax.set_xlim(lims)
    # ax.set_ylim(lims)
    #
    # ax = plt.subplot(222)
    # plt.scatter(res.fittedvalues, res.resid)
    # plt.title('Residuals vs Fitted')
    # plt.xlabel("Fitted")
    # plt.ylabel('Residuals')
    #
    # ax = plt.subplot(223)
    # pb = sm.ProbPlot(res.resid)
    # pb.qqplot(ax=ax)
    # sm.qqline(ax, 's', x=pb.theoretical_quantiles, y=pb.sample_quantiles)
    # # ax.title('QQ Plot')
    #
    # ax = plt.subplot(224)
    # sm.graphics.influence_plot(res, ax=ax, size=12)
    #
    # plt.tight_layout()
    # plt.subplots_adjust(top=0.85)
    # file.savefig()
    # plt.close()

def lassopath(model, varname=None, file=None):
    """Lasso CV paths"""

    m_log_alphas = -np.log10(model.lassocv.alphas_)
    m_log_alpha = -np.log10(model.lassocv.alpha_)

    # Compute the path again. Seems quite unefficient but lassoCV doesnt
    # provide the coefficients
    coef_path = np.zeros((len(model.lassocv.coef_), len(model.lassocv.alphas_)))
    for i, alp in enumerate(model.lassocv.alphas_):
        lassot = Lasso(alpha=alp, fit_intercept=True).fit(model._preds, model._vardata)
        coef_path[:, i] = lassot.coef_

    coef_path = coef_path.T

    plt.figure(figsize=(9, 6))

    ax = plt.gca()
    plt.subplot(2, 1, 1)
    plt.plot(m_log_alphas, coef_path)
    plt.axvline(-np.log10(model.lassocv.alpha_), linestyle='--', color='k',
                label='alpha: CV estimate')

    plt.title(varname)
    plt.ylabel('weights')
    plt.axis('tight')
    
    plt.subplot(2, 1, 2)
    ymin, ymax = np.min(model.lassocv.mse_path_), np.max(model.lassocv.mse_path_)
    plt.plot(m_log_alphas, model.lassocv.mse_path_, ':')
    plt.plot(m_log_alphas, model.lassocv.mse_path_.mean(axis=-1), 'k',
             label='Average across the folds', linewidth=2)
    plt.axvline(-np.log10(model.lassocv.alpha_), linestyle='--', color='k',
                label='alpha: CV estimate')

    plt.legend()

    plt.xlabel('-log(alpha)')
    plt.ylabel('MSE on each fold')
    plt.axis('tight')
    plt.ylim(ymin, ymax)

    plt.tight_layout()
    file.savefig()
    plt.close()

def glacier_ts(var, ofile):
    """The timeseries in a GlacierVar object"""

    toplot = [{'d':var.ref_ts, 'ls':'-', 'lab':'ref'},
              {'d':var.fullmodel_ts, 'ls':'--', 'lab':'fullmodel'}]

    if var.outercv_ts is not None:
        toplot.append({'d':var.outercv_ts, 'ls':'--', 'lab':'outer cv'})
    elif var.cv_ts is not None:
        toplot.append({'d': var.cv_ts, 'ls':'--', 'lab':'cv'})

    __timeserie(var.ref_ts.index.date, toplot, title= '{} at {}'.format(var.name, var.zlevel),
                         yunits=var.units, file=ofile)

def scattervar(ref, fitted, time, varname=None, vartype=None, file=None):
    """OLS regression plots"""

    plt.figure(figsize=(9, 6))

    plt.suptitle('{}: ref vs {}'.format(varname, vartype))

    ax = plt.subplot(221)
    plt.scatter(ref, fitted)
    plt.title('Model vs Fitted (R2={0:.2f})'.format(dgstats.cor2(ref, fitted)))
    plt.xlabel('Observations')
    plt.ylabel('Fitted')

    lims = [np.min([ax.get_xlim(), ax.get_ylim()]),  # minmax of both axes
            np.max([ax.get_xlim(), ax.get_ylim()])]

    # now plot both limits against each other
    ax.plot(lims, lims, 'k-', alpha=0.75, zorder=0)
    ax.set_xlim(lims)
    ax.set_ylim(lims)

    ax = plt.subplot(222)
    plt.scatter(fitted, fitted-ref)
    plt.title('Residuals vs Fitted')
    plt.xlabel('Fitted')
    plt.ylabel('Residuals')

    ax = plt.subplot(223)
    pb = sm.ProbPlot(fitted-ref)
    pb.qqplot(ax=ax)
    sm.qqline(ax, 's', x=pb.theoretical_quantiles, y=pb.sample_quantiles)

    ax = plt.subplot(224)
    plt.plot_date(time, fitted-ref, color='blue')

    plt.title('Residuals vs Time')
    plt.xlabel('Time')
    plt.ylabel('Residuals')
    ax.xaxis.set_major_locator(YearLocator())
    ax.xaxis.set_major_formatter(DateFormatter('%Y'))
    ax.xaxis.set_minor_locator(MonthLocator())

    plt.tight_layout()
    plt.subplots_adjust(top=0.85)
    file.savefig()
    plt.close()