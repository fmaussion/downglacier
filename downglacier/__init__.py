from os import path

from sklearn.utils import ConvergenceWarning
import warnings
warnings.filterwarnings("ignore", category=ConvergenceWarning)

# Path to the file directory
file_dir = path.join(path.abspath(path.dirname(__file__)), 'files')