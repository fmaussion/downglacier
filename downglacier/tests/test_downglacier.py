import os, unittest
import numpy.random as random

import downglacier
import downglacier.conf as dgconf
import downglacier.stats as dgstats
import downglacier.dgio as dgio
import numpy as np
import netCDF4 as nc
import datetime as date
import shutil
import pandas as pd
from configobj import ConfigObj
import statsmodels.api as sm
from statsmodels.tools import eval_measures as em
from sklearn.cross_validation import KFold
from matplotlib import mlab
import downglacier.main as dg
import csv
import logging as log

def randominds(nv, ni):
    """ ni random indices out of nv possibilities. """
    out = np.arange(nv)
    random.shuffle(out)
    return out[:ni]


def make_simple_predfile(path):
    """Very basic predictor file for test_read_predictors"""

    nco = nc.Dataset(path, 'w')
    nco.info = 'Template predictor file for testing purposes'
    nco.date = date.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    nt = 24
    nco.createDimension('time', nt)

    # Time
    units = 'hours since 1900-01-01 00:00:00'
    v = nco.createVariable('time', 'l', ('time',))
    v.long_name = 'Time'
    v.units = units
    v[:] = np.arange(nt)

    # Variables
    for i, vn in enumerate(['a', 'b', 'c']):
        v = nco.createVariable(vn, 'f4', ('time',))
        v.long_name = 'Dummy variable {}'.format(vn)
        v.units = 'UnitS'
        v[:] = np.arange(nt)**i

    nco.close()


def make_perfect_predfile(path, tpl_path):

    sebnc = nc.Dataset(tpl_path, mode='r')
    to = sebnc.variables['time']
    time = nc.num2date(to[:],units=to.units)
    zlevels = sebnc.variables['z'][:]
    nt = len(time)

    # We make it double period to test predictions
    dtime = np.append(time - date.timedelta(days=365*4+1), time)

    ## Write out
    nco = nc.Dataset(path, 'w')
    nco.info = 'Template predictor file for testing purposes'
    nco.date = date.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    nco.createDimension('time', len(dtime))

    # Time
    units = 'hours since 1900-01-01 00:00:00'
    dd = nc.date2num(dtime, units)
    v = nco.createVariable('time', 'l', ('time',))
    v.long_name = 'Time'
    v.units = units
    v[:] = dd

    # Variables
    rkeys = ['LWin', 'LWout', 'sfacc', 'swiasky', 'SWout', 'QL', 'QS', 'QPS',
             'QC', 'tempgrid', 'vpgrid', 'vgrid', 'refsmass', 'suimass', 'submelt_int']
    for vn in rkeys:
        for i, z in enumerate(zlevels):
            zvn = vn + '_{}'.format(int(z))
            v = nco.createVariable(zvn, 'f4', ('time',))
            v.long_name = 'Dummy variable {}'.format(zvn)
            v.units = 'UnitS'
            v[:] = np.append(sebnc.variables[vn][:, i], sebnc.variables[vn][:, i])

    zvn = 'MBSpec'
    vn = 'MBSpec'
    v = nco.createVariable(zvn, 'f4', ('time',))
    v.long_name = 'Dummy variable {}'.format(zvn)
    v.units = 'UnitS'
    da = sebnc.variables[vn][:]
    da = da - np.append(0., da[0:-1])
    v[:] = np.append(da, da)

    nco.close()

def make_pcr_predfile(path, tpl_path):

    sebnc = nc.Dataset(tpl_path, mode='r')

    to = sebnc.variables['time']
    time = nc.num2date(to[:],units=to.units)
    zlevels = np.atleast_1d(sebnc.variables['z'][:][0])

    # We make it double period to test predictions
    dtime = np.append(time - date.timedelta(days=365*4+1), time)
    nt = len(dtime)

    ## Write out
    nco = nc.Dataset(path, 'w')
    nco.info = 'Template predictor file for testing purposes'
    nco.date = date.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

    nco.createDimension('time', len(dtime))

    # Time
    units = 'hours since 1900-01-01 00:00:00'
    dd = nc.date2num(dtime, units)
    v = nco.createVariable('time', 'l', ('time',))
    v.long_name = 'Time'
    v.units = units
    v[:] = dd

    # Variables
    rkeys = ['LWin', 'LWout', 'sfacc', 'swiasky', 'SWout', 'QL', 'QS', 'QPS',
             'QC', 'tempgrid', 'vpgrid', 'vgrid', 'refsmass', 'suimass', 'submelt_int']
    v = np.zeros((48,len(rkeys)))
    for n, vn in enumerate(rkeys):
        for i, z in enumerate(zlevels):
            v[:, n] = sebnc.variables[vn][:, i]

    pca = mlab.PCA(v)
    for n, vn in enumerate(rkeys):
        v = nco.createVariable('pc_{:02d}'.format(n+1), 'f4', ('time',))
        v.long_name = 'Dummy variable {}'.format(n+1)
        v.units = 'UnitS'
        v[:] = np.append(pca.Y[:, n], pca.Y[:,n])

    nco.close()

class TestAlgos(unittest.TestCase):

    def setUp(self):
        pass

    def test_l1oclim(self):

        inp = np.append(np.ones(12), np.ones(12)*2)
        exp = np.append(np.ones(12)*2, np.ones(12))
        out = dgstats.l1o_climatology(inp)
        np.testing.assert_almost_equal(out, exp)

        inp = np.append(np.append(np.ones(12), np.ones(12)*2), np.ones(12)*3)
        exp = np.append(np.append(np.ones(12)*2.5, np.ones(12)*2), np.ones(12)*1.5)
        out = dgstats.l1o_climatology(inp)
        np.testing.assert_almost_equal(out, exp)

    def test_collin(self):

        npred = 30
        nobs = 10000

        # set up
        vns = ['{:02}'.format(s) for s in range(npred)]
        data = pd.DataFrame(random.rand(nobs, npred), columns=vns)

        # Practically impossible to have collinear predictors with 1000 obs
        res = dgstats.find_collinearity(data)
        for t, v in res.items():
            self.assertTrue(v is None)

        # set up
        data[vns[28]] = data[vns[26]]
        data[vns[29]] = data[vns[27]]
        res = dgstats.find_collinearity(data)
        for t, v in res.items():
            if t == '0.98_1.0':
                self.assertTrue((('26','28') in v) or (('28','26') in v))
                self.assertTrue((('27','29') in v) or (('29','27') in v))

    def test_crossval(self):

        npred = 3
        nobs = 50

        # set up
        preds = sm.add_constant(pd.DataFrame(random.rand(nobs, npred))).values

        # perfect model
        params = [3., -1., 2., 4.]
        X = np.dot(params, preds.T)
        self.assertTrue(X.shape[0] == nobs)

        selp = np.arange(npred+1)

        ols = dgstats.OLSModel(selp)
        ols.fit(X, preds)
        np.testing.assert_allclose(ols.fittedvalues, X)
        np.testing.assert_allclose(ols.model.params, params)

        # in a perfect model the l1o ts should be exact same
        crossvalite = dgstats.LeaveWinOutIte(len(X), 2)
        ts = dgstats.crossval_ts(X, preds, ols, crossvalite)
        np.testing.assert_allclose(ts, X)

        # non-perfect model test
        # repeat because of random effects
        repeat = 20
        for i in range(repeat):
            # add a bit of random
            _X = X + random.rand(nobs) * 0.2
            ols = dgstats.OLSModel(selp)
            ols.fit(_X, preds)
            modts = ols.fittedvalues
            crossvalite = dgstats.LeaveWinOutIte(len(_X), 2)
            l1ots = dgstats.crossval_ts(_X, preds, ols, crossvalite)

            # the l1o RMSE should always be larger than full model
            self.assertTrue(em.rmse(_X, modts) <= em.rmse(_X, l1ots))

        # non-perfect model test other iterator
        # repeat because of random effects
        for i in range(repeat):
            # add a bit of random
            _X = X + random.rand(nobs) * 0.2
            ols = dgstats.OLSModel(selp)
            ols.fit(_X, preds)
            modts = ols.fittedvalues
            crossvalite = KFold(len(_X), 5)
            l1ots = dgstats.crossval_ts(_X, preds, ols, crossvalite)

            # the l1o RMSE should always be larger than full model
            self.assertTrue(em.rmse(_X, modts) <= em.rmse(_X, l1ots))

        # non-perfect model test lasso
        # repeat because of random effects
        for i in range(repeat):
            # add a bit of random
            _X = X + random.rand(nobs) * 0.2
            ols = dgstats.LassoModel(0.1)
            ols.fit(_X, preds)
            modts = ols.fittedvalues
            crossvalite = KFold(len(_X), 5)
            l1ots = dgstats.crossval_ts(_X, preds, ols, crossvalite)

            # the l1o RMSE should always be larger than full model
            self.assertTrue(em.rmse(_X, modts) <= em.rmse(_X, l1ots))

    def test_dummyscreenmodel(self):

        npred = 3
        nobs = 48

        time = pd.date_range('1/1/2011', periods=48, freq='M')

        # set up
        preds = pd.DataFrame(random.rand(nobs, npred), index=time)

        # perfect model
        params = [3., 0., 2., 0.]
        X = pd.Series(np.dot(params, sm.add_constant(preds).T), index=time)
        self.assertTrue(X.shape[0] == nobs)

        gv = dgio.GlacierVar(ref_ts=X)
        totest = dgstats.DummyModelSelector(gv, preds)
        totest.compute_fullmodel()
        np.testing.assert_allclose(totest.fullmodel_ts, X)
        totest.compute_cv()
        np.testing.assert_allclose(totest.cv_ts, X)
        totest.compute_outercv()
        np.testing.assert_allclose(totest.outercv_ts, X)

        # non-perfect model test
        # repeat because of random effects
        repeat = 20
        for i in range(repeat):
            # add a bit of random
            _X = X + random.rand(nobs) * 0.2
            gv = dgio.GlacierVar(ref_ts=_X)
            totest = dgstats.DummyModelSelector(gv, preds)
            totest.compute_fullmodel()
            totest.compute_cv()
            totest.compute_outercv()

            # the l1o RMSE should be larger than full model
            self.assertTrue(em.rmse(_X, totest.fullmodel_ts) <= em.rmse(_X, totest.cv_ts))
            # and the outer RMSE?
            self.assertTrue(em.rmse(_X, totest.cv_ts) <= em.rmse(_X, totest.outercv_ts))

    def test_modelselectors(self):

        random.seed(1309)

        npred = 15
        nobs = 48
        time = pd.date_range('1/1/2011', periods=48, freq='M')
        vns = np.array(['{:02}'.format(n+1) for n in np.arange(npred)])

        # Delete possible colinearities to make their job easier
        pca = mlab.PCA(random.rand(nobs, npred))
        preds = pd.DataFrame(pca.Y, index=time, columns=vns)

        # a few valid predictors to the model
        nval = 6
        chosen = randominds(npred, nval)
        vnref = vns[chosen]

        # perfect model
        params = np.clip(random.rand(nval+1) * 10, 2, 10)
        params[[1,3]] = -params[[1,3]]

        Y = sm.add_constant(preds[vnref])
        X = np.dot(params, Y.T)

        # see if I understood what i did:
        ols = sm.OLS(X, Y)
        _ = ols.fit()
        self.assertTrue(ols.rank, npred+1)
        fit = ols.fit()
        np.testing.assert_allclose(fit.fittedvalues, X)
        np.testing.assert_allclose(fit.params, params)

        # let them run and see if they find the solution
        mods = [
                dgstats.CrossvalModelSelector,
                dgstats.PartialCorModelSelector,
                dgstats.LassoModelSelector,
                dgstats.RelaxedLassoModelSelector,
                dgstats.OLSLassoModelSelector,
                dgstats.ConstrainedLassoModelSelector
                ]
        tols = [1e-5, 1e-5, 0.1, 0.1, 1e-5, 0.1] # lasso is not exact
        for model, tol in zip(mods, tols):
            gv = dgio.GlacierVar(ref_ts=pd.Series(X, index=time))
            totest = model(gv, preds)
            totest.compute_fullmodel()
            np.testing.assert_allclose(totest.fullmodel_ts, X, rtol=tol, atol=tol)
            totest.compute_cv()
            np.testing.assert_allclose(totest.cv_ts, X, rtol=tol, atol=tol)
            totest.compute_outercv()
            np.testing.assert_allclose(totest.outercv_ts, X, rtol=tol, atol=tol)

            # Are the names right?
            selp, tp = totest.selpreds_summary()
            # For some reason, random changed
            if not (model is dgstats.PartialCorModelSelector):
                np.testing.assert_equal(np.sort(selp), np.sort(vnref))
            # add some random
            _X = X + random.rand(nobs) * 40 - 20
            gv = dgio.GlacierVar(ref_ts=pd.Series(_X, index=time))
            totest = model(gv, preds)

            totest.compute_fullmodel()
            modts = totest.fullmodel_ts
            totest.compute_cv()
            ts = totest.cv_ts

            # the l1o RMSE should always be larger than full model
            self.assertTrue(em.rmse(_X, modts) <= em.rmse(_X, ts))

            # And the outer l1o RMSE?
            totest.compute_outercv()
            its = totest.outercv_ts
            self.assertTrue(em.rmse(_X, ts) - em.rmse(_X, its) <= 0.01,
                            'should: {} <= {}'.format(em.rmse(_X, ts),
                                                      em.rmse(_X, its)))


class TestIO(unittest.TestCase):

    def setUp(self):

        log.basicConfig(format='%(asctime)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
        level=log.WARNING)

        # resource directory
        self.resdir = downglacier.file_dir
        self.sebf = os.path.join(self.resdir, 'seb_template.nc')

        # test directory
        self.testdir = os.path.join(os.path.dirname(__file__), 'tmp')
        if not os.path.exists(self.testdir):
            os.makedirs(self.testdir)
        self.clean_dir()

        ## Make a simple predictor file
        self.simplepredf = os.path.join(self.testdir, 'simple_predictor_template.nc')
        make_simple_predfile(self.simplepredf)

        ## Make a "prefect predictor file"
        self.perfectpredf = os.path.join(self.testdir, 'perfect_predictor_template.nc')
        make_perfect_predfile(self.perfectpredf, self.sebf)

        ## Make a prefect predictor file from PCs
        self.pcrpredf = os.path.join(self.testdir, 'pcr_predictor_template.nc')
        make_pcr_predfile(self.pcrpredf, self.sebf)

        ## Test file for init
        cfgin = os.path.join(self.resdir, 'defaults.cfg')
        cfgin = ConfigObj(cfgin, file_error=True)
        cfgin['I/O']['working_dir'] = self.testdir
        cfgin['I/O']['predictor_file'] = self.perfectpredf
        cfgin['I/O']['predictand_file'] = self.sebf
        self.cfgfile = os.path.join(self.testdir, 'dummy.cfg')
        cfgin.filename = self.cfgfile
        cfgin.write()

    def rm_dir(self):
        shutil.rmtree(self.testdir)

    def clean_dir(self):
        shutil.rmtree(self.testdir)
        os.makedirs(self.testdir)

    def test_read_sst(self):

        from downglacier.sandbox import tcd_utils
        from datetime import datetime as dt

        sst_df = tcd_utils.get_all_sst_df()

        self.assertTrue(sst_df['mi'].iloc[0] == 1)
        self.assertTrue(sst_df['yi'].iloc[0] == 1980)
        self.assertTrue(sst_df['mi'].iloc[-1] == 12)
        self.assertTrue(sst_df['yi'].iloc[-1] == 2013)

        slice = sst_df.loc[dt(1979, 10, 1)]
        ref = np.mean([-0.03, 0.15, 0.23, -0.08, -0.27])
        np.testing.assert_allclose(slice['NINO1.2'], ref, atol=1e-6)
        ref = np.mean([-0.11, 0.27, 0.09, 0.33, 0.46])
        np.testing.assert_allclose(slice['NINO3.4'], ref, atol=1e-6)

        slice = sst_df.loc[dt(2005, 5, 1)]
        ref = np.mean([0.53, 0.39, 0.45, 0.24, -0.09])
        np.testing.assert_allclose(slice['NINO3.4'], ref, atol=1e-6)
        ref = np.mean([1.032, .589, .801, .519, .492])
        np.testing.assert_allclose(slice['MEI'], ref, atol=1e-6)

        sst_df_l = tcd_utils.get_all_sst_df(lag=3)
        new_slice = sst_df_l.loc[dt(2005, 8, 1)]
        np.testing.assert_allclose(slice[0:3], new_slice[0:3], atol=1e-6)

    def test_nino_class(self):

        from downglacier.sandbox import tcd_utils
        from datetime import datetime as dt

        sst_df = tcd_utils.get_enso_df(longest=True)

        ref = [False] + [True] * 9 + [False]
        mine = sst_df.loc[dt(1972, 5, 1):dt(1973, 3, 1)].is_nino.values
        np.testing.assert_equal(ref, mine)

    def test_read_predictors(self):

        df = dgio.read_predictor_file(self.simplepredf, normalize=False)
        # Data frame
        self.assertTrue(df.a.mean() == 1.)
        self.assertTrue(df.c['1900-01-01 23:00:00'] == 23**2)
        totest = df.b['1900-01-01 16:00:00':'1900-01-01 20:00:00']
        np.testing.assert_equal(totest, np.arange(16, 21, 1))

        # Metadata
        self.assertTrue(df.b.long_name == 'Dummy variable b')
        self.assertTrue(df.c.long_name == 'Dummy variable c')
        self.assertTrue(df.a.units == 'UnitS')

        # And with normalization?
        df = dgio.read_predictor_file(self.simplepredf, normalize=True)
        np.testing.assert_almost_equal(df.b.mean(), 0)

        # Lag?
        df = dgio.read_predictor_file(self.simplepredf, normalize=False, lag=1)
        self.assertTrue(len(df.keys()) == 6)
        self.assertTrue('b_lag1' in df)
        self.assertTrue(len(df.index) == 23)
        self.assertTrue(str(df.index[0]) == '1900-01-01 01:00:00')
        np.testing.assert_equal(df.c[0:-1].values, df.c_lag1[1:].values)
        df = dgio.read_predictor_file(self.simplepredf, normalize=False, lag=2)
        self.assertTrue(len(df.keys()) == 9)
        self.assertTrue('b_lag1' in df)
        self.assertTrue('b_lag2' in df)
        self.assertTrue(len(df.index) == 22)
        self.assertTrue(str(df.index[0]) == '1900-01-01 02:00:00')
        np.testing.assert_equal(df.c[0:-1].values, df.c_lag1[1:].values)
        np.testing.assert_equal(df.c[1:-1].values, df.c_lag1[2:].values)

        # close
        self.rm_dir()


    def test_read_predictands(self):

        vkeys = {'tempgrid': 'airtemp'}
        ikeys ={'a_sc': 'area'}
        cont, idf = dgio.read_predictand_file(self.sebf,
                                              vkeys=vkeys,
                                              ikeys=ikeys)
        np.testing.assert_equal(cont.zlevels, np.arange(4700, 5900, 100))

        ifile = nc.Dataset(self.sebf)
        cont, idf = dgio.read_predictand_file(self.sebf,
                                              zlevels=4800,
                                              vkeys=vkeys,
                                              ikeys=ikeys)
        np.testing.assert_equal(cont.zlevels, 4800)
        np.testing.assert_equal(list(cont.keys()), ['airtemp_4800_ds'])
        np.testing.assert_equal(cont.getvar('airtemp', 4800).ref_ts,
                                ifile.variables['tempgrid'][:,1])

        cont, idf = dgio.read_predictand_file(self.sebf,
                                              zlevels=[4700, 5400],
                                              vkeys=vkeys,
                                              ikeys=ikeys)
        np.testing.assert_equal(cont.zlevels, [4700, 5400])
        np.testing.assert_equal(list(sorted(cont.keys())),
                                ['airtemp_4700_ds', 'airtemp_5400_ds'])
        np.testing.assert_equal(cont.getvar('airtemp', 4700).ref_ts,
                                ifile.variables['tempgrid'][:,0])
        np.testing.assert_equal(cont.getvar('airtemp', 5400).ref_ts,
                                ifile.variables['tempgrid'][:,7])
        ifile.close()


        # Full package
        cp = ConfigObj(self.cfgfile, file_error=True)
        vkeys = dict((v.strip().lower(), k.strip().lower())
                 for k in cp['Vkeys'] for v in cp['Vkeys'].as_list(k))
        ikeys = dict((v.strip().lower(), k.strip().lower())
                 for k in cp['Ikeys'] for v in cp['Ikeys'].as_list(k))

        cont, idf = dgio.read_predictand_file(self.sebf,
                                              vkeys=vkeys,
                                              ikeys=ikeys)

        self.assertTrue('mbspec' in cont.varnames)
        self.assertTrue('prcptot' in cont.varnames)
        self.assertTrue('subsurfmass' in cont.varnames)
        self.assertFalse('prcpliq' in cont.varnames)
        self.assertFalse('suimass' in cont.varnames)

        p = cont.getvar(name='mbspec', zlevel=0)
        self.assertTrue(p.units == 'mm w.e. month^-1')

        da = pd.DataFrame({'ref_ts': p.ref_ts, 'l1o': p.l1o_climatology})
        da['month'] = np.arange(1,13,1).tolist() * 4
        da['y'] =  np.zeros(12*4)
        da.loc[0:12, 'y'] = 1
        np.testing.assert_equal(da.l1o[0],
                                da.ref_ts[(da.month == 1) & (da.y == 0)].mean())
        np.testing.assert_equal(da.l1o[2],
                                da.ref_ts[(da.month == 3) & (da.y == 0)].mean())

        da['y'] =  np.zeros(12*4)
        da.loc[12:24, 'y'] = 1
        np.testing.assert_equal(da.l1o[12],
                                da.ref_ts[(da.month == 1) & (da.y == 0)].mean())
        np.testing.assert_equal(da.l1o[14],
                                da.ref_ts[(da.month == 3) & (da.y == 0)].mean())

        # close
        self.rm_dir()

    def test_init(self):

        dgconf.initialize(self.cfgfile)

        cont, idf, pred = dgconf.varcont, dgconf.dfinfo, dgconf.dfpred

        self.assertTrue('mbspec' in cont.varnames)
        self.assertTrue('prcptot' in cont.varnames)
        self.assertTrue('subsurfmass' in cont.varnames)
        self.assertFalse('prcpliq' in cont.varnames)
        self.assertFalse('suimass' in cont.varnames)


    def test_perfectds(self):

        cp = ConfigObj(self.cfgfile, file_error=True)
        cp['I/O']['working_dir'] = self.testdir
        cp['I/O']['predictor_file'] = self.perfectpredf
        cp['I/O']['predictand_file'] = self.sebf
        if 'keep_zlevels' in cp['I/O']:
            del cp['I/O']['keep_zlevels']
        cp['I/O']['multiprocessing'] = True
        cp['I/O']['processes'] = -1

        del cp['Screen']
        cp['Screen'] = {}
        cp['Screen']['run'] = True
        cp['Screen']['compute_cv'] = True
        cp['Screen']['compute_outercv'] = False

        cp['Screen']['outercv'] = 'w1'

        cp['Screen']['algo'] = 'dummy'

        cp['Screen']['Keywords'] = {}

        del cp['CheckInput']
        cp['CheckInput'] = {}
        cp['CheckInput']['run'] = False

        del cp['Regstats']
        cp['Regstats'] = {}
        cp['Regstats']['run'] = False
        cp['Regstats']['plots'] = False

        del cp['Diagnostic']
        cp['Diagnostic'] = {}
        cp['Diagnostic']['run'] = True
        cp['Diagnostic']['plots'] = False

        cfgfile = os.path.join(self.testdir, 'dummy.cfg')
        cp.filename = cfgfile
        cp.write()
        dg.workflow(cfgfile)

        fi = os.path.join(self.testdir, 'diagnostics', 'results.csv')
        cnt = 0
        cnt2 = 0
        with open(fi) as csvfile:
            r = csv.reader(csvfile)
            for row in r:
                if 'mbtotfromsum' in row[0]:
                    self.assertTrue(float(row[-1]) >= 0.96)
                    cnt += 1
                if 'mbspec' in row[0]:
                    self.assertTrue(float(row[-1]) >= 0.96)
                    cnt2 += 1
        self.assertTrue(cnt == 12)
        self.assertTrue(cnt2 == 5)

        fi = os.path.join(self.testdir, 'diagnostics', 'fullmodel_ts.csv')
        dff = pd.read_csv(fi)
        fi = os.path.join(self.testdir, 'diagnostics', 'predict_ts.csv')
        dfp = pd.read_csv(fi)

        np.testing.assert_almost_equal(dfp['airtemp_4900_ds'].values[48:],
                                       dfp['airtemp_4900_ds'].values[0:48])

        np.testing.assert_almost_equal(dff['airtemp_4900_ds'].values,
                                       dfp['airtemp_4900_ds'].values[0:48])

        nco = nc.Dataset(self.perfectpredf, mode='r')

        v = nco.variables['tempgrid_4900']
        np.testing.assert_almost_equal(v[:], dfp['airtemp_4900_ds'].values)

        v = nco.variables['MBSpec']
        self.assertTrue(em.rmse(v[:], dfp['mbspecfromcfsum_0_diag'].values) < 6.)

        nco.close()

        self.rm_dir()

    def test_perfectpcr(self):

        algos = []
        keywds = []

        algos.append('stepwise_regression_pcor')
        keywds.append({'threshold':0.1, 'maxpred':15})

        algos.append('stepwise_regression_cvrmse')
        keywds.append({'threshold':0.01, 'maxpred':15, 'selectorcv':'k4'})

        algos.append('lasso')
        keywds.append({'selectorcv':'k4'})

        algos.append('relaxed_lasso')
        keywds.append({'selectorcv':'k4'})

        algos.append('lasso_ols')
        keywds.append({'selectorcv':'k4'})

        algos.append('constrained_lasso')
        keywds.append({'selectorcv':'k4', 'maxpred':15})

        tols = [1e-5, 1e-5, 0.1, 0.1, 0.1, 0.1] # lasso is not exact
        for algo, keywd, tol in zip(algos, keywds, tols):

            cp = ConfigObj(self.cfgfile, file_error=True)
            cp['I/O']['working_dir'] = self.testdir
            cp['I/O']['predictor_file'] = self.pcrpredf
            cp['I/O']['predictand_file'] = self.sebf
            if 'keep_zlevels' in cp['I/O']:
                del cp['I/O']['keep_zlevels']
                cp['I/O']['keep_zlevels'] = 4700
            cp['I/O']['multiprocessing'] = True
            cp['I/O']['processes'] = -1

            del cp['Screen']
            cp['Screen'] = {}
            cp['Screen']['run'] = True
            cp['Screen']['compute_cv'] = True
            cp['Screen']['compute_outercv'] = False

            cp['Screen']['outercv'] = 'w1'

            cp['Screen']['algo'] = algo
            cp['Screen']['Keywords'] = keywd

            del cp['CheckInput']
            cp['CheckInput'] = {}
            cp['CheckInput']['run'] = False

            del cp['Regstats']
            cp['Regstats'] = {}
            cp['Regstats']['run'] = True
            cp['Regstats']['plots'] = False

            del cp['Diagnostic']
            cp['Diagnostic'] = {}
            cp['Diagnostic']['run'] = True
            cp['Diagnostic']['plots'] = False

            cfgfile = os.path.join(self.testdir, 'dummy.cfg')
            cp.filename = cfgfile
            cp.write()
            dg.workflow(cfgfile)

            fi = os.path.join(self.testdir, 'diagnostics', 'results.csv')
            cnt = 0
            with open(fi) as csvfile:
                r = csv.reader(csvfile)
                for row in r:
                    if 'mbtotfromsum' in row[0]:
                        self.assertTrue(float(row[-1]) >= 0.96)
                        cnt += 1
            self.assertTrue(cnt == 1)

            fi = os.path.join(self.testdir, 'diagnostics', 'fullmodel_ts.csv')
            dff = pd.read_csv(fi)
            fi = os.path.join(self.testdir, 'diagnostics', 'predict_ts.csv')
            dfp = pd.read_csv(fi)

            np.testing.assert_almost_equal(dfp['airtemp_4700_ds'].values[48:],
                                           dfp['airtemp_4700_ds'].values[0:48])

            np.testing.assert_almost_equal(dff['airtemp_4700_ds'].values,
                                           dfp['airtemp_4700_ds'].values[0:48])

            nco = nc.Dataset(self.perfectpredf, mode='r')

            v = nco.variables['tempgrid_4700']
            np.testing.assert_allclose(v[:], dfp['airtemp_4700_ds'].values, rtol=tol, atol=tol)

            nco.close()

        self.rm_dir()


    def test_recuay(self):

        from datetime import datetime as dt
        import warnings

        file = os.path.join(self.resdir, 'prcp_recuay_monthly.csv')
        df = pd.read_csv(file, parse_dates=0, index_col=0)

        file = os.path.join(self.resdir, 'tcd_paper', 'standard_run_predictors_era.nc')
        preds = dgio.read_predictor_file(file)
        preds = preds.loc[dt(1979, 10, 1): dt(2011, 9, 1)]

        locs = [(dt(2005, 10, 1), dt(2009, 9, 1)),
                (dt(2001, 10, 1), dt(2009, 9, 1))]
        exps = ['2005_2009',
                '2001_2009']
        oudf = df.copy()
        sdf = pd.DataFrame()
        for lo, exp in zip(locs, exps):

            tdf = df.loc[lo[0]:lo[1]].copy()
            gv = dgio.GlacierVar(ref_ts=tdf.prcp)
            totest = dgstats.LassoModelSelector(gv, preds.loc[lo[0]:lo[1]],
                                                outercv='w2',
                                                selectorcv='k4')

            totest.compute_fullmodel()

            its = np.clip(totest.fullmodel_ts, 0, np.max(totest.fullmodel_ts))
            fm_rmse = em.rmse(tdf.prcp, its)
            sdf.loc[exp, 'fm_rmse'] = fm_rmse
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                totest.compute_outercv()
            its = np.clip(totest.outercv_ts, 0, np.max(totest.outercv_ts))
            ds_rmse = em.rmse(tdf.prcp, its)
            sdf.loc[exp, 'cv_noclip_rmse'] = em.rmse(tdf.prcp, totest.outercv_ts)
            sdf.loc[exp, 'cv_rmse'] = ds_rmse

            pts = totest.predict(preds)
            pts =  np.clip(pts, 0, np.max(pts))
            oudf[exp] =  pts
            predict_rmse = em.rmse(oudf.prcp, pts)
            sdf.loc[exp, 'predict_rmse'] = predict_rmse

            # we check that the RMSE we computed is close to the "real" RMSE
            # in the 4 years case it is 0.95, which is not too bad even
            # if should be higher
            self.assertTrue((ds_rmse / predict_rmse) > 0.9)


        # this was for external tests
        oudf.to_csv(os.path.join(self.testdir, 'recuay_ds.csv'))
        sdf.to_csv(os.path.join(self.testdir, 'recuay_ds_scores.csv'))

        self.rm_dir()


if __name__ == '__main__':

    unittest.main()