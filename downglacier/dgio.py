""" Input/Output functions

Copyright: Fabien Maussion, 2014-2015

License: GPLv3+
"""
from collections import OrderedDict
import os
import netCDF4 as nc
import numpy as np
import pandas as pd
from matplotlib import mlab
from statsmodels.tools import eval_measures as em
import calendar

# Locals
import downglacier.stats as stats


def read_predictor_file(path, normalize=True, lag=0, pc=None,
                        normalize_period=None):

    """Reads the predictor file (netcdf or csv).

    Parameters
    ----------
    path: path to the file to read
    normalize: normalize the predictors over the time period (default: True)
    lag: add lagged predictors of lag l. If lag=2, 1 **and** 2 lagged
         predictors are added
    pc: convert the predictors to principle components
        setting this parameter to a negative value or to none
        wont make any transformation, setting it to 1 will
        select all the pcs, selecting it to a float between
        0.95 and 1. is the most standard approach. Default: None

    Returns
    -------
    a DataFrame (incl. metadata)"""

    if not os.path.isfile(path):
        raise IOError(path)

    _, fileExtension = os.path.splitext(path)

    if fileExtension.lower() == '.csv':
        raise NotImplementedError('CSV files not implemented (yet)')

    elif fileExtension.lower() == '.nc':
        # open and read time
        nco = nc.Dataset(path, mode='r')
        time = nco.variables['time']
        time = nc.num2date(time[:], units=time.units)
        # Loop over the variables
        vnames = []
        metadata = dict()
        data = np.array([])
        for vn, v in nco.variables.items():
            if vn == 'time': continue # we don't want time
            data = np.append(data, v[:])
            vnames.append(vn)
            metadata[vn] = {'long_name': v.long_name,
                            'units': v.units}
        # Store in a DataFrame
        data = data.reshape(len(vnames), len(time)).T
        df = pd.DataFrame(data, index=time, columns=vnames)
        nco.close()

    else:
        raise IOError('File extension not understood: ' + fileExtension)

    if lag != 0:
        # User wants lagged time series
        k = df.keys()
        for l in np.arange(1, lag+1, 1):
            for c in k:
                id = c + '_lag{}'.format(l)
                df[id] = df[c].shift(l)
                metadata[id] = {'long_name': metadata[c]['long_name'] + \
                                            ' lag {}'.format(l),
                                 'units': metadata[c]['units']}
        df = df.dropna()

    if normalize:
        if normalize_period is None:
            df = (df - df.mean()) / df.std()
        else:
            p = normalize_period
            df = (df - df.loc[p[0]:p[1]].mean()) / df.loc[p[0]:p[1]].std()

    # Add metadata to the columns
    for c in df:
        df[c].long_name = metadata[c]['long_name']
        df[c].units = metadata[c]['units']

    # Principle component conversion
    if pc is not None:
        if normalize_period is None:
            pca = mlab.PCA(df)
        else:
            p = normalize_period
            pca = mlab.PCA(df.loc[p[0]:p[1]])

        vals = df.values
        proj = vals * 0
        for i, vec in enumerate(vals):
            proj[i, :] = pca.project(vec)

        if normalize_period is not None:
            # TODO: assertion not necessary on the long term
            dft = pd.DataFrame(proj, index=df.index)
            assert np.all(pca.Y == dft.loc[p[0]:p[1]])

        pstop = np.where(np.cumsum(pca.fracs) <= pc)[0]
        pcnames = ['pc_{:02d}'.format(n+1) for n in np.arange(len(pstop))]
        df = pd.DataFrame(proj[:, pstop], columns=pcnames, index=time)
        for c in df:
            df[c].long_name = c
            df[c].units = c

    return df

def read_predictand_file(path, vkeys=None, ikeys=None, zlevels=None):
    """Reads the predictand file (netcdf).

    Additionally, it renames variables to the DownGlacier convention, adds
    some diagnostic variables and removes useless ones.

    Parameters
    ----------
    path: path to the file
    vkeys: variables keymap
    ikeys: info variables keymap
    zlevels: selected zlevels

    Returns
    -------
    (VarContainer object, info DataFrame) tuple
    """

    if not os.path.isfile(path):
        raise IOError(path)

    _, fileExtension = os.path.splitext(path)

    nco = nc.Dataset(path, mode='r')
    time = nco.variables['time']
    time = nc.num2date(time[:], units=time.units)

    # default z levels
    zl = nco.variables['z'][:]
    if zlevels is None:
        zinds = np.arange(len(zl))
    else:
        zlevels = np.atleast_1d(zlevels)
        zinds = np.array([np.where(zl==z)[0][0] for z in zlevels])
        zl = zl[zinds]

    # Info dataframe + metadata
    infodf = pd.DataFrame()
    # Temporary dict to fill
    vs = dict()
    for vn, v in nco.variables.items():
        vn = vn.lower()
        if vn == 'time': continue # we don't want time
        if vn in ikeys:
            s = pd.Series(v[zinds], index=zl)
            s.long_name = v.long_name
            s.units = v.units
            infodf[ikeys[vn]] = s
        if vn in vkeys:
            vn = vkeys[vn] # change varname
            if v[:].ndim == 2:
                data = v[:, zinds]
            elif v[:].ndim == 1:
                data = v[:] # this is really just for mbspec
            vs[vn] = {'ref_ts':data, 'long_name':v.long_name, 'units':v.units}

    nco.close()

    # add some diagvars
    if 'submelt' in vs and 'refsmass' in vs and 'suimass' in vs:
        da = vs['refsmass']['ref_ts'] + vs['suimass']['ref_ts'] - vs['submelt']['ref_ts']
        ln = 'Subsurface mass change'
        un = vs['refsmass']['units']
        vs['subsurfmass'] = {'ref_ts':da, 'long_name':ln, 'units':un}
        del vs['submelt'], vs['refsmass'], vs['suimass']

    if 'swin' in vs and 'swout' in vs:
        da = vs['swin']['ref_ts'] + vs['swout']['ref_ts']
        ln = 'Net shortwave radiation'
        un = vs['swin']['units']
        vs['swnet'] =  {'ref_ts':da, 'long_name':ln, 'units':un}

    if 'lwin' in vs and 'lwout' in vs:
        da = vs['lwin']['ref_ts'] + vs['lwout']['ref_ts']
        ln = 'Net longwave radiation'
        un = vs['lwin']['units']
        vs['lwnet'] =  {'ref_ts':da, 'long_name':ln, 'units':un}

    if 'prcpliq' in vs and 'prcpsol' in vs:
        da = vs['prcpliq']['ref_ts'] + vs['prcpsol']['ref_ts']
        ln = 'Total precipitation'
        un = vs['prcpsol']['units']
        vs['prcptot'] = {'ref_ts':da, 'long_name':ln, 'units':un}
        del vs['prcpliq']

    if 'mbspec' in vs:
        # Deaccumulate MBSpec (TODO: ugly de-accumulate)
        da = vs['mbspec']['ref_ts']
        da = da - np.append(0., da[0:-1])
        vs['mbspec']['ref_ts'] = da
        vs['mbspec']['units'] = 'mm w.e. month^-1'

    # Now fill the container
    cont = VarContainer(time, zl)
    for vn, v in vs.items():
        data = v['ref_ts']
        if data.ndim == 2:
            for (i, z) in enumerate(zl):
                long_name = v['long_name'] + ' at {} m height'.format(z)
                ser = pd.Series(data[:, i], index=time)
                cont.addvar(GlacierVar(ref_ts=ser, units=v['units'],
                                       name=vn, zlevel=z,
                                       long_name=long_name, isdiag=False))
        elif data.ndim == 1:
            # this is really just for mbspec
            long_name = v['long_name']
            ser = pd.Series(data[:], index=time)
            cont.addvar(GlacierVar(ref_ts=ser, units=v['units'],
                                   name=vn, zlevel=0,
                                   long_name=long_name, isdiag=False))

    return cont, infodf


class VarContainer(dict):
    """Container for GlacierVar instances.

    Defines a convention for the key names: name_zlevel_type.
    """

    def __init__(self, time, zlevels):
        """Instanciated once only, time and zlevels are simple holders

        Parameters
        ----------
        time: the time array
        zlevels: the glacier zlevels
        """
        self.time = time
        self.zlevels = zlevels
        self.varnames = set()

    def addvar(self, var):
        """Adds a variable to the dictionary"""
        vtype = 'diag' if var.isdiag else 'ds'
        key = '{}_{}_{}'.format(var.name, var.zlevel, vtype)
        if key in self:
            raise ValueError('Key already taken: {}'.format(key))
        self.varnames.add(var.name)
        self[key] = var
        return key

    def getvar(self, name=None, zlevel=None, isdiag=False):
        """Gets a variable to the dictionary"""
        vtype = 'diag' if isdiag else 'ds'
        if zlevel is None: zlevel = 0
        key = '{}_{}_{}'.format(name, zlevel, vtype)
        return self[key]

    def dfdata(self, vartype='ref_ts'):
        """The data for all vars in a dataframe.

        Parameters
        ----------
        vartype: the type of data you want: 'ref_ts', 'cv_ts',
                 'outercv_ts', 'predict_ts' or 'fullmodel_ts'
        """
        df = pd.DataFrame()
        for vn, v in sorted(self.items()):
            df[vn] = getattr(v, vartype)
        for vn, v in sorted(self.items()):
            df[vn].long_name = v.long_name
            df[vn].units = v.units
        return df




class GlacierVar():
    """Handles variables during the whole program's lifetime.

    Mostly a wrapper of the underlying ModelSelector, but with
    Pandas nicer tools.
    """

    def __init__(self, ref_ts=None, name='', zlevel=0,
                 units='', long_name='', isdiag=False):
        """ Create a variable from scratch."""

        self.ref_ts = ref_ts
        self.name = name
        self.zlevel = zlevel
        self.units = units
        self.long_name = long_name
        self.isdiag = isdiag
        self.l1o_climatology = pd.Series(data=stats.l1o_climatology(ref_ts),
                                         index=ref_ts.index)

        # This is filled by screen() OR by compute_diagnostics()
        self.model = None


    @property
    def fullmodel_ts(self):
        """Fullmodel timeserie"""
        return pd.Series(data=self.model.fullmodel_ts, index=self.ref_ts.index)

    @property
    def cv_ts(self):
        """Inner cv (bad) cross-validation timeserie"""
        if self.model.cv_ts is None:
            return None
        return pd.Series(data=self.model.cv_ts, index=self.ref_ts.index)

    @property
    def outercv_ts(self):
        """Outer cv (good) cross-validation timeserie"""
        if self.model.outercv_ts is None:
            return None
        return pd.Series(self.model.outercv_ts, index=self.ref_ts.index)

    def predict(self, predsdf):
        """Predict."""
        self.model.predict_ts = pd.Series(data=self.model.predict(predsdf),
                                          index=predsdf.index)

    @property
    def predict_ts(self):
        """Prediction timeserie"""
        return self.model.predict_ts


    def skillscores(self):
        """Computes the variable's skill scores.

        Returns
        -------
        an OrderedDict
        """

        out = OrderedDict()
        out['varname'] = self.name

        selp, toprint = self.model.selpreds_summary()
        out['nselpreds'] = len(selp)
        out['selpreds'] = toprint

        out['mean'] = np.mean(self.ref_ts)
        out['stddev'] = np.std(self.ref_ts)

        # Fill only the scores which are available
        tses = [self.fullmodel_ts, self.cv_ts, self.outercv_ts]
        labels = ['fullmodel', 'cv', 'outercv']
        for ts, label in zip(tses, labels):
            if ts is None: continue
            out[label + '_rmse'] = em.rmse(self.ref_ts, ts)
            out[label + '_r2'] = stats.cor2(self.ref_ts, ts)
            out[label + '_percsigma'] = out[label + '_rmse'] / out['stddev']
            out[label + '_skillscore'] = stats.brierss(self.ref_ts,
                                                         self.l1o_climatology,
                                                         ts)
        return out

    def info(self):
        """Get the variable's information.

        Returns
        -------
        an OrderedDict
        """

        out = OrderedDict()

        out['name'] = self.name
        out['zlevel'] = self.zlevel
        out['unit'] =  self.units
        out['long_name'] = self.long_name
        out['isdiag'] = self.isdiag

        tses = [self.fullmodel_ts, self.cv_ts, self.outercv_ts]
        labels = ['fullmodel', 'cv', 'outercv']
        for ts in tses:
            if ts is None: continue
            out['rmse'] = em.rmse(self.ref_ts, ts)

        return out



def compute_diagnostics(varcont, cv=None, user_z=False, dfinfo=None):
    """Compute the SEB/SMB variables from downscaled data.

    It's quite a monolithic function, basically re-doing the budget
    calculations of Moelg et al's model. This core routine
    should be adapted to other models if this becomes necessary one day."""

    def diagvar(varname, zlevel, newvarname):
        """Adds an empty diagnostic variable to the container"""
        tmp = varcont.getvar(name=varname, zlevel=zlevel)
        out = GlacierVar(ref_ts=tmp.ref_ts, name=newvarname, zlevel=zlevel,
                 units=tmp.units, long_name=tmp.long_name, isdiag=True)
        out.model = stats.DiagnosticModel()
        varcont.addvar(out)
        return out

    # Check if the necessary varables are present
    needed = ['swin', 'lwin', 'swout', 'lwout', 'qs', 'ql', 'qc', 'prcpsol',
              'qps', 'subsurfmass', 'qm', 'mbtot', 'meltmass', 'submass']
    if not set(needed).issubset(set(varcont.varnames)):
        return

    # Constants
    lm = 334000.  # enthalpy of fusion, J kg-1
    lvsub = 2.848e6  # [J/kg] heat of subli
    dtstep = 60 * 60  # secs in hour

    # Go
    for zl in varcont.zlevels:

        # Downscaled data
        swin = varcont.getvar(name='swin', zlevel=zl)
        lwin = varcont.getvar(name='lwin', zlevel=zl)
        swout = varcont.getvar(name='swout', zlevel=zl)
        lwout = varcont.getvar(name='lwout', zlevel=zl)
        qs = varcont.getvar(name='qs', zlevel=zl)
        ql = varcont.getvar(name='ql', zlevel=zl)
        prcpsol = varcont.getvar(name='prcpsol', zlevel=zl)
        qc = varcont.getvar(name='qc', zlevel=zl)
        qps = varcont.getvar(name='qps', zlevel=zl)
        subsurfmass = varcont.getvar(name='subsurfmass', zlevel=zl)
        swnet = varcont.getvar(name='swnet', zlevel=zl)
        lwnet = varcont.getvar(name='lwnet', zlevel=zl)
        if 'prcptot' in varcont.varnames:
            prcptot = varcont.getvar(name='prcptot', zlevel=zl)
            prcptotclip = diagvar('prcptot', zl, 'prcptotclip')

        # Define empty diagnostic vars
        prcpsolclip = diagvar('prcpsol', zl, 'prcpsolclip')

        swnetsum = diagvar('swnet', zl, 'swnetsum')
        lwnetsum = diagvar('lwnet', zl, 'lwnetsum')

        qmfromsum = diagvar('qm', zl, 'qmfromsum')
        qmfromnet = diagvar('qm', zl, 'qmfromnet')

        meltmassfromsum = diagvar('meltmass', zl, 'meltmassfromsum')
        meltmassfromnet = diagvar('meltmass', zl, 'meltmassfromnet')
        meltmassfromsumcf = diagvar('meltmass', zl, 'meltmassfromsumcf')
        meltmassfromnetcf = diagvar('meltmass', zl, 'meltmassfromnetcf')

        submass = diagvar('submass', zl, 'submass')
        submasscf = diagvar('submass', zl, 'submasscf')

        mbtotfromcfsum = diagvar('mbtot', zl, 'mbtotfromcfsum')
        mbtotfromcfnet = diagvar('mbtot', zl, 'mbtotfromcfnet')
        mbtotfromsum = diagvar('mbtot', zl, 'mbtotfromsum')
        mbtotfromnet = diagvar('mbtot', zl, 'mbtotfromnet')

        # Fill diag vars for each level
        vts = ['fullmodel_ts', 'cv_ts', 'outercv_ts', 'predict_ts']
        for vt in vts:

            def g(var):
                """Shortcut for getattr"""
                return getattr(var, vt)

            def s(var, val):
                """Shortcut for setattr"""
                setattr(var.model, vt, val)

            def corfac(varout, varin):
                """Computes the correction factor."""
                if vt == 'predict_ts': # cf was computed in a preceeding loop
                    s(varout, g(varin) * varout.model.corfac)
                    return
                corf = stats.correction_factor(g(varin), varin.ref_ts, vt, cv=cv)
                if vt == 'fullmodel_ts':
                    varout.model.corfac = corf # use model as container for later
                s(varout, g(varin) * corf)

            # Do we have the level?
            if getattr(swin, vt) is None:
                continue

            # hours in month
            if vt == 'predict_ts':
                himo = [calendar.monthrange(t.year, t.month)[1] for t in swin.predict_ts.index]
                himo = 24 * np.array(himo)
            else:
                himo = [calendar.monthrange(t.year, t.month)[1] for t in varcont.time]
                himo = 24 * np.array(himo)

            # Clipped precip
            s(prcpsolclip, np.clip(g(prcpsol), 0, np.max(g(prcpsol))))
            if 'prcptot' in varcont.varnames:
                s(prcptotclip, np.clip(g(prcptot), 0, np.max(g(prcptot))))

            # Net SW
            s(swnetsum, g(swin) + g(swout))

            # Net LW
            s(lwnetsum, g(lwin) + g(lwout))

            # Melt energy
            s(qmfromnet, g(swnet) + g(lwnet) + g(qs) + g(ql) + g(qps) + g(qc))
            s(qmfromnet, np.clip(g(qmfromnet), 0, np.max(g(qmfromnet))))

            s(qmfromsum, g(swnetsum) + g(lwnetsum) + g(qs) + g(ql) + g(qps) + g(qc))
            s(qmfromsum, np.clip(g(qmfromsum), 0, np.max(g(qmfromsum))))

            # Melting mass
            s(meltmassfromsum, g(qmfromsum) / lm * dtstep * himo)
            s(meltmassfromnet, g(qmfromnet) / lm * dtstep * himo)

            corfac(meltmassfromsumcf, meltmassfromsum)
            corfac(meltmassfromnetcf, meltmassfromnet)

            # Sublimation mass
            s(submass, np.where(g(ql) < 0, -g(ql), 0) / lvsub * dtstep * himo)
            corfac(submasscf, submass)

            # Total MB
            s(mbtotfromcfsum, g(prcpsolclip) - g(meltmassfromsumcf) - g(submasscf) + g(subsurfmass))
            s(mbtotfromcfnet, g(prcpsolclip) - g(meltmassfromnetcf) - g(submasscf) + g(subsurfmass))
            s(mbtotfromsum, g(prcpsolclip) - g(meltmassfromsum) - g(submass) + g(subsurfmass))
            s(mbtotfromnet, g(prcpsolclip) - g(meltmassfromnet) - g(submass) + g(subsurfmass))

    # Add specific MB if possible
    if not user_z and 'mbspec' in varcont.varnames:

        mbspecfromcfsum = diagvar('mbspec', 0, 'mbspecfromcfsum')
        mbspecfromcfnet = diagvar('mbspec', 0, 'mbspecfromcfnet')
        mbspecfromsum = diagvar('mbspec', 0, 'mbspecfromsum')
        mbspecfromnet = diagvar('mbspec', 0, 'mbspecfromnet')
        mbspecfromds = diagvar('mbspec', 0, 'mbspecfromds')

        norarea = dfinfo['area']
        norarea = norarea / np.sum(norarea)

        # downscaled level
        vts = ['fullmodel_ts', 'cv_ts', 'outercv_ts', 'predict_ts']
        for vt in vts:

            def g(var):
                return getattr(var, vt)

            def s(var, val):
                setattr(var.model, vt, val)

            # Do we have the level?
            if getattr(swin, vt) is None:
                continue

            nt = len(getattr(swin, vt))
            nz = len(varcont.zlevels)
            mbtotfromcfsum = np.zeros((nt, nz))
            mbtotfromcfnet = np.zeros((nt, nz))
            mbtotfromsum = np.zeros((nt, nz))
            mbtotfromnet = np.zeros((nt, nz))
            mbtotfromds = np.zeros((nt, nz))

            area = np.repeat(np.atleast_2d(norarea).T, nt, axis=1).T

            for i, zl in enumerate(varcont.zlevels):
                mbtotfromcfsum[:, i] = g(varcont.getvar(name='mbtotfromcfsum', zlevel=zl, isdiag=True))
                mbtotfromcfnet[:, i] = g(varcont.getvar(name='mbtotfromcfnet', zlevel=zl, isdiag=True))
                mbtotfromsum[:, i] = g(varcont.getvar(name='mbtotfromsum', zlevel=zl, isdiag=True))
                mbtotfromnet[:, i] = g(varcont.getvar(name='mbtotfromnet', zlevel=zl, isdiag=True))
                mbtotfromds[:, i] = g(varcont.getvar(name='mbtot', zlevel=zl, isdiag=False))

            s(mbspecfromcfsum, np.sum(mbtotfromcfsum * area, axis=1))
            s(mbspecfromcfnet, np.sum(mbtotfromcfnet * area, axis=1))
            s(mbspecfromsum, np.sum(mbtotfromsum * area, axis=1))
            s(mbspecfromnet, np.sum(mbtotfromnet * area, axis=1))
            s(mbspecfromds, np.sum(mbtotfromds * area, axis=1))